#!/usr/bin/python3
from flask import Flask, flash, request, redirect, url_for
from flask_cors import CORS
import os
import json
from werkzeug.utils import secure_filename

import tokenizer
"""
pip requirements:
spacy

then to start run:
export FLASK_APP=main.py
export FLASK_DEBUG=1 (optional, to reload upon changes automatically)
python -m flask run

example calls:

curl -X GET localhost:5000/detectLanguages?input="aap"

curl -F 'pdffile=@temp_pdf_storage/machine_readable_single_column_2.pdf' -X POST localhost:5000/ocr



"""


app = Flask(__name__)
app.secret_key = "super secret key"
CORS(app)


@app.route('/welcome', methods=['GET'])
def dummy():
    return "Hello stranger, can you tell us where you've been?\nMore importantly, how ever did you come to be here?\n"




##################### Language Identification #####################
@app.route('/tokenize', methods=['GET'])
def tokenize():

    """
    Required args:
    - informat (turtle, txt, etc.)
    - outformat (turtle, other nif-usuals)
    - input (actual input text to spot entities in)

    Optional arg:
    - language (currently only en and de supported)
    """
    
    supported_informats = ['txt']
    supported_informats.extend(tokenizer.SUPPORTED_NIFFORMATS)
    supported_outformats = tokenizer.SUPPORTED_NIFFORMATS
    inp = None
    if request.args.get('input') == None:
        if request.data == None:
            return 'Please provide some text as input.\n'
        else:
            inp = request.data.decode('utf-8')
    else:
        inp = request.args.get('input')
    if request.args.get('informat') == None:
        return 'Please specify input format (currently supported: %s)\n' % str(supported_informats)
    elif request.args.get('informat') not in supported_informats:
        return 'Input format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('informat'), (str(supported_informats)))
    if request.args.get('outformat') == None:
        return 'Please specify output format (currently supported: %s)\n' % str(supported_outformats)
    elif request.args.get('outformat') not in supported_outformats:
        return 'Output format "%s" not among supported formats. Please picke one from: %s.\n' % (request.args.get('outformat'), (str(supported_outformats)))

    informat = request.args.get('informat')
    outformat = request.args.get('outformat')

    lang = 'multi'
    if request.args.get('input') == None:
        return 'Please provide some text as input.\n'
    if request.args.get('lang') != None:
        lang = request.args.get('lang')

    nifString = tokenizer.tokenize(inp, informat, outformat, lang)
        
    return nifString


    
if __name__ == '__main__':

    port = int(os.environ.get('PORT',5000))
    app.run(host='localhost', port=port, debug=True)
