FROM python:3.6-stretch
LABEL maintainer="peter.bourgonje@dfki.de"


RUN apt-get -y update && \
    apt-get upgrade -y && \
    apt-get install -y python3-dev &&\
    apt-get update -y


ADD requirements.txt .
RUN pip3 install -r requirements.txt

ADD tokenizer.py .
ADD nif.py .
ADD flaskController.py .

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

EXPOSE 8080

ENTRYPOINT FLASK_APP=flaskController.py flask run --host=0.0.0.0 --port=8080
#CMD ["/bin/bash"]
