import spacy
from nif import Nif
from spacy.lang.en import English
from spacy.lang.de import German



SUPPORTED_NIFFORMATS = ['turtle', 'xml', 'json-ld']

def tokenizeToList(inp, informat, outformat, lang):

    nlp = None
    if lang == 'multi':
        nlp = spacy.load("xx_ent_wiki_sm")
    elif lang == 'en':
        nlp = English()
    elif lang == 'de':
        nlp = German()
    
    tok = nlp.Defaults.create_tokenizer(nlp)
    nifmodel = Nif()

    if informat == 'txt':
        nifmodel.initNifModelFromString(inp)
    elif informat in SUPPORTED_NIFFORMATS:
        nifmodel.parseModelFromString(inp, informat)
    else:
        nifmodel.initNifModelFromString(inp)
        sys.stderr.write('ERROR: Input format "%s" not supported. Interpreting input as plaintext.\n' % informat)
    plaintextinput = nifmodel.extractIsString()

    tokens = tok(plaintextinput)

    return tokens



def tokenize(inp, informat, outformat, lang):

    nlp = None
    if lang == 'multi':
        nlp = spacy.load("xx_ent_wiki_sm")
    elif lang == 'en':
        nlp = English()
    elif lang == 'de':
        nlp = German()
    
    tok = nlp.Defaults.create_tokenizer(nlp)
    nifmodel = Nif()

    if informat == 'txt':
        nifmodel.initNifModelFromString(inp)
    elif informat in SUPPORTED_NIFFORMATS:
        nifmodel.parseModelFromString(inp, informat)
    else:
        nifmodel.initNifModelFromString(inp)
        sys.stderr.write('ERROR: Input format "%s" not supported. Interpreting input as plaintext.\n' % informat)
    plaintextinput = nifmodel.extractIsString()

    tokens = tok(plaintextinput)

    nifmodel.addTokens(tokens)

    nifString = None
    if outformat in SUPPORTED_NIFFORMATS:
        nifString = nifmodel.model.serialize(format=outformat).decode('utf-8')
    else:
        sys.stderr.write('ERROR: "turtle" is the only supported output format for now. Returning None, will probably crash afterwards...\n')
    
    return nifString


#ns = tokenize('bla bla bla.', 'txt', 'turtle')
#print(ns)
