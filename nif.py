from rdflib import Namespace, Graph, URIRef, XSD, RDF, Literal, plugin
#from rdflib.serializer import Serializer
from rdflib.plugin import Serializer, register
import time
import tempfile
import sys
import os
import re
from collections import defaultdict

register('json-ld', Serializer, 'rdflib_jsonld.serializer', 'JsonLDSerializer')
                           
KNOWN_ENTITY_TYPES = set(['http://dbpedia.org/ontology/Person',
                          'http://dbpedia.org/ontology/Miscellaneous',
                          'http://dbpedia.org/ontology/Location',
                          'http://dbpedia.org/ontology/Organisation'])

class Nif():

    def __init__(self):
        self.NIF = Namespace('http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#')
        self.ITSRDF = Namespace('http://www.w3.org/2005/11/its/rdf#')
        self.quratorns = Namespace("http://qurator.ai")
        self.model = Graph()

    def addTokens(self, tokens):

        isstring = self.extractIsString()
        for token in tokens:
            beginIndex = token.idx
            endIndex = token.idx+len(token)
            anchorOf = isstring[beginIndex:endIndex]
            annuri = URIRef("%s#char=%i,%i" % (self.docuri, beginIndex, endIndex))
            self.model.add((annuri, RDF.type, self.NIF.String))
            #self.model.add((annuri, self.NIF.anchorOf, Literal(anchorOf)))
            self.model.add((annuri, self.NIF.beginIndex, Literal(beginIndex, datatype=XSD.nonNegativeInteger)))
            self.model.add((annuri, self.NIF.endIndex, Literal(endIndex, datatype=XSD.nonNegativeInteger)))
            self.model.add((annuri, self.ITSRDF.taClassRef, Literal('spacy.Token')))

    def getTokens(self):

        isstr = self.extractIsString()
        tokens = [s for s, p, o in self.model.triples((None, None, Literal('spacy.Token')))]
        tokenspans = []
        for i, t in enumerate(tokens):
            start = None
            end = None
            for s, p, o in self.model.triples((t, self.NIF.beginIndex, None)):
                start = int(o)
            for s, p, o in self.model.triples((t, self.NIF.endIndex, None)):
                end = int(o)
            tokenspans.append((start,end))
        tokenspans = sorted(tokenspans, key = lambda x: x[0])
        texttokens = [isstr[t[0]:t[1]] for t in tokenspans]

        return texttokens
        

    def replaceDoubleQuotes(self):

        isstruri = self.isstruri
        isstr = self.extractIsString()
        #self.model.remove((isstruri, RDF.type, Literal(isstr)))
        self.model.add((isstruri, RDF.type, Literal(re.sub('"', '\'', isstr))))
        

    def initNifModelFromString(self, isString):

        isString = re.sub('"', '\'', isString)
        self.model.bind('nif', self.NIF)
        self.model.bind('itsrsdf', self.ITSRDF)
        textlen = len(isString)
        self.docuri = URIRef('%s/documents/%s' % (self.quratorns, str(time.time())))
        self.isstruri = URIRef('%s#char=%i,%i' % (self.docuri, 0, textlen))
        self.model.add((self.isstruri, RDF.type, self.NIF.Context))
        self.model.add((self.isstruri, RDF.type, self.NIF.String))
        self.model.add((self.isstruri, self.NIF.isString, Literal(isString)))
        self.model.add((self.isstruri, self.NIF.beginIndex, Literal(0, datatype=XSD.nonNegativeInteger)))
        self.model.add((self.isstruri, self.NIF.endIndex, Literal(textlen, datatype=XSD.nonNegativeInteger)))

    
    def extractIsString(self):

        isString = None
        if not (None, self.NIF.isString, None) in self.model:
            sys.stderr.write('ERROR: isString not found in nifstring. Not returning anything.\n')
        else:
            for s, p, o in self.model.triples((None, self.NIF.isString, None)): # should be only one, but found no way of getting it directly from generator object
                isString = str(o)
        return isString

    def parseModelFromString(self, nifstring, informat):

        # really stupid: there does not seem to be a way to rdflib.Graph().parse from string, so have to write to tempfile here
        fd, fn = tempfile.mkstemp()
        f = os.fdopen(fd, 'w')
        f.write(nifstring)
        f.close()
        if informat in ['turtle', 'xml', 'json-ld']:
            #self.model = Graph().parse(fn, format='n3')
            self.model = Graph().parse(fn, format=informat)
            subjects = set([re.sub('#char=.*$', '', x) for x in self.model.subjects()])
            rawsubjects = set([x for x in self.model.subjects()])
            if len(subjects) < 1:
                sys.stderr.write('ERROR: Could not find unique document uri. Very hacky way of getting this, which obviously failed at this point. Killing now.\n')
                sys.exit()
            self.docuri = list(subjects)[0]
            self.isstruri = list(rawsubjects)[0]
            self.replaceDoubleQuotes()

        else:
            sys.stderr.write('ERROR: Input format "%s" not supported. Not returning anything.\n' % informat)

        os.remove(fn)

    def extractEntityLabels(self):

        known_ents = set()
        for s, p, o in self.model.triples((None, self.ITSRDF.taClassRef, None)):
            if str(o) in KNOWN_ENTITY_TYPES:
                known_ents.add(s)
        uri2label = defaultdict(str)
        labels = set()
        for s, p, o in self.model.triples((None, self.NIF.anchorOf, None)):
            if s in known_ents:
                labels.add(str(o))
                uri2label[str(s)] = str(o)
        return labels, uri2label
            

    def addURIToEntity(self, s, uri):

        self.model.add((s, self.ITSRDF.taIdentRef, Literal(uri)))
                        


if __name__ == '__main__':

    samplenif = """
@prefix itsrsdf: <http://www.w3.org/2005/11/its/rdf#> .
@prefix nif: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<http://qurator.ai/documents/1566481330.803265#char=0,130> a nif:Context,
        nif:String ;
    nif:beginIndex "0"^^xsd:nonNegativeInteger ;
    nif:endIndex "132"^^xsd:nonNegativeInteger ;
    nif:isString \"""
    Ich bin in Berlin, Julian ist in Madrid.   

    Kommt dir Julian Moreno Schneider bekannt   vor?
    Und was mit "Georg Rehm"?\""" .

<http://qurator.ai/documents/1566481330.803265#char=119,129> a nif:String ;
    nif:anchorOf "Georg Rehm" ;
    nif:beginIndex "120"^^xsd:nonNegativeInteger ;
    nif:endIndex "130"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "http://dbpedia.org/ontology/Person" .

<http://qurator.ai/documents/1566481330.803265#char=16,22> a nif:String ;
    nif:anchorOf "Berlin" ;
    nif:beginIndex "16"^^xsd:nonNegativeInteger ;
    nif:endIndex "22"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "http://dbpedia.org/ontology/Location" .

<http://qurator.ai/documents/1566481330.803265#char=38,44> a nif:String ;
    nif:anchorOf "Madrid" ;
    nif:beginIndex "38"^^xsd:nonNegativeInteger ;
    nif:endIndex "44"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "http://dbpedia.org/ontology/Location" .

<http://qurator.ai/documents/1566481330.803265#char=64,87> a nif:String ;
    nif:anchorOf "Julian Moreno Schneider" ;
    nif:beginIndex "64"^^xsd:nonNegativeInteger ;
    nif:endIndex "87"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "http://dbpedia.org/ontology/Person" .
    """
    informat = 'turtle'
    n2 = Nif()
    nifmodel2 = n2.parseModelFromString(samplenif, informat)

    labels = n2.extractEntityLabels()
    
    #isstr = n2.extractIsString()
    #print('isstr here:', isstr)
    

    
    ser = n2.model.serialize(format='json-ld') # turtle, try rdfxml, jsonld (https://ropensci.github.io/rdflib/reference/rdf_serialize.html)
    # working so far; turtle, xml, json-ld. For json-ld, do a pip3 install rdflib-jsonld

    
    #print(ser.decode('utf-8'))

    tokensnif = """
@prefix itsrsdf: <http://www.w3.org/2005/11/its/rdf#> .
@prefix nif: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

<http://qurator.ai/documents/1597230384.5075536#char=0,12> a nif:Context,
        nif:String ;
    nif:beginIndex "0"^^xsd:nonNegativeInteger ;
    nif:endIndex "12"^^xsd:nonNegativeInteger ;
    nif:isString "bla bla bla." .

<http://qurator.ai/documents/1597230384.5075536#char=0,3> a nif:String ;
    nif:anchorOf "bla" ;
    nif:beginIndex "0"^^xsd:nonNegativeInteger ;
    nif:endIndex "3"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "spacy.Token" .

<http://qurator.ai/documents/1597230384.5075536#char=11,12> a nif:String ;
    nif:anchorOf "." ;
    nif:beginIndex "11"^^xsd:nonNegativeInteger ;
    nif:endIndex "12"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "spacy.Token" .

<http://qurator.ai/documents/1597230384.5075536#char=4,7> a nif:String ;
    nif:anchorOf "bla" ;
    nif:beginIndex "4"^^xsd:nonNegativeInteger ;
    nif:endIndex "7"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "spacy.Token" .

<http://qurator.ai/documents/1597230384.5075536#char=8,11> a nif:String ;
    nif:anchorOf "bla" ;
    nif:beginIndex "8"^^xsd:nonNegativeInteger ;
    nif:endIndex "11"^^xsd:nonNegativeInteger ;
    itsrsdf:taClassRef "spacy.Token" .
"""
    n3 = Nif()
    nifmodel3 = n3.parseModelFromString(tokensnif, 'turtle')
    tokens = n3.getTokens()
    print(tokens)
    
